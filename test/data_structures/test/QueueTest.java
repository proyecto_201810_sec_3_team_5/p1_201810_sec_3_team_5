package data_structures.test;

import junit.framework.*;
import model.data_structures.*;
import model.vo.*;

public class QueueTest<T extends Comparable <T>> extends TestCase {

	private Queue<Taxi> q;

	private void setupEscenario1(){
		
		q = new Queue<Taxi>();
		
		Taxi taxi1 = new Taxi("A123","A");
		Taxi taxi2 = new Taxi("C345","D");
		Taxi taxi3 = new Taxi("R567","F");
		Taxi taxi4 = new Taxi("T678","J");
		Taxi taxi5 = new Taxi("Y901","P");
		
		q.enqueue(taxi1);
		q.enqueue(taxi2);
		q.enqueue(taxi3);
		q.enqueue(taxi4);
		q.enqueue(taxi5);
	}
	
	private void setupEscenario2(){
		q = new Queue<Taxi>();
	}
	
	public void testEnqueue(){
		setupEscenario1();
		Taxi taxiC = new Taxi("Z999","L");
		q.enqueue(taxiC);
		if(q.darUltimo().darElemento().compareTo(taxiC)!=0){
			fail("El elemento al final de la cola no es el correcto");
		}
	}
	
	public void testDequeue(){
		setupEscenario1();
		Taxi taxiC = q.dequeue();
		Taxi taxiE = new Taxi("A123","A");
		if(q.darPrimero().darElemento().compareTo(taxiE)==0 || taxiC.compareTo(taxiE)!=0){
			fail("El elemento al inicio de la cola no es el correcto");
		}
		
		setupEscenario2();
		if(q.dequeue() != null){
			fail("La cola est� vac�a, dequeue deber�a retornar null.");
		}
	}
	
	public void testSize(){
		setupEscenario1();
		if(q.size()!=5){
			fail("El tama�o de la cola no es el correcto");
		}
	}
	public void testIsEmpty(){
		setupEscenario1();
		if(q.isEmpty()==true){
			fail("La cola no est� vac�a.");
		}
		
		setupEscenario2();
		if(q.isEmpty()==false){
			fail("La cola est� vac�a.");
		}
	}
	
}
