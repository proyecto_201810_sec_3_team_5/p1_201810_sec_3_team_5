package data_structures.test;

import junit.framework.*;
import model.data_structures.*;
import model.vo.*;

public class ListTest<T extends Comparable <T>> extends TestCase {
	private List<Taxi> lista;

	private void setupEscenario1(){
		lista = new List<Taxi>();
		Taxi taxi1 = new Taxi("A123","A");
		Taxi taxi2 = new Taxi("C345","D");
		Taxi taxi3 = new Taxi("R567","F");
		Taxi taxi4 = new Taxi("T678","J");
		Taxi taxi5 = new Taxi("Y901","P");
		
		lista.agregar(taxi1);
		lista.agregar(taxi2);
		lista.agregar(taxi3);
		lista.agregar(taxi4);
		lista.agregar(taxi5);
	}
	
	public void testDarPrimero(){
		Taxi taxiComp = new Taxi("A123","A");
		setupEscenario1();
		if(lista.darPrimero().darElemento().compareTo(taxiComp)!=0){
			fail("El primer elemento no es el correcto");
		}
	}
	
	public void testDarUltimo(){
		Taxi taxiComp2 = new Taxi("Y901","P");
		setupEscenario1();
		if(lista.darUltimo().darElemento().compareTo(taxiComp2)!=0){
			fail("El ultimo elemento no es el correcto");
		}
	}
	
	public void testDarLongitud(){
		setupEscenario1();
		if(lista.darLongitud() != 5){
			fail("La lista no tiene la longitud correcta.");
		}
	}
	
	public void testAgregar(){
		setupEscenario1();
		Taxi taxiComp = new Taxi("Z888","ZZZ");
		lista.agregar(taxiComp);
		if(lista.darLongitud()!=6 && !lista.darUltimo().darElemento().equals(taxiComp)){
			fail("El elemento no se agreg� correctamente");
		}
	}
	
	public void testEliminar(){
		setupEscenario1();
		Taxi taxiComp = new Taxi("Y901","P");
		Taxi taxiComp2 = new Taxi("A123","A");
		Taxi taxiComp3 = new Taxi("R567","F");
		
		lista.eliminar(taxiComp);
		if(lista.eliminar(taxiComp)==false){
			fail("El ultimo elemento no se elimin� correctamente");
		}
		
		setupEscenario1();
		lista.eliminar(taxiComp2);
		if(lista.darLongitud()>=5 || lista.eliminar(taxiComp2)==false){
			fail("El primer elemento no se elimin� correctamente");
		}
		
		setupEscenario1();
		lista.eliminar(taxiComp3);
		if(lista.darLongitud()>=5 || lista.eliminar(taxiComp3)==false){
			fail("El elemento no se elimin� correctamente");
		}
	}
}
