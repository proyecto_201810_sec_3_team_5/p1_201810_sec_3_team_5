package data_structures.test;

import junit.framework.*;
import model.data_structures.*;
import model.vo.*;

public class StackTest<T extends Comparable <T>> extends TestCase {

	private Stack<Taxi> stack;

	private void setupEscenario1(){
		
		stack = new Stack<Taxi>();
		
		Taxi taxi1 = new Taxi("A123","A");
		Taxi taxi2 = new Taxi("C345","D");
		Taxi taxi3 = new Taxi("R567","F");
		Taxi taxi4 = new Taxi("T678","J");
		Taxi taxi5 = new Taxi("Y901","P");
		
		stack.push(taxi1);
		stack.push(taxi2);
		stack.push(taxi3);
		stack.push(taxi4);
		stack.push(taxi5);
	}
	
	private void setupEscenario2(){
		stack = new Stack<Taxi>();
	}
	
	public void testPush(T elemento) {
		setupEscenario1();
		Taxi taxiC = new Taxi("ZZ32","K");
		stack.push(taxiC);
		if(stack.getLongitud()!= 6 || stack.darPrimero().darElemento().compareTo(taxiC)!=0){
			fail("El elemento encima de la pila no es el correcto.");
		}
	}

	public void testPop() {
		setupEscenario1();
		Taxi taxiC = new Taxi("A123","A");
		stack.pop();
		if(stack.getLongitud()!= 4 || stack.darPrimero().darElemento().compareTo(taxiC)==0){
			fail("El elemento encima de la pila no es el correcto.");
		}
		
		setupEscenario2();
		if(stack.pop()!=null){
			fail("No deber�a retornar algo porque la pila est� vac�a.");
		}
	}

	public void testIsEmpty() {
		setupEscenario1();
		if(stack.isEmpty()==true){
			fail("La pila no est� vac�a.");
		}
		
		setupEscenario2();
		if(stack.isEmpty()==false){
			fail("La pila est� vac�a.");
		}
	}
	
	public void testGetLongitud(){
		setupEscenario1();
		if(stack.getLongitud()!=5){
			fail("El tama�o de la pila no es el correcto");
		}
	}
}
