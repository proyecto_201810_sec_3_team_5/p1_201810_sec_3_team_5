package model.vo;

import model.data_structures.*;

public class FechaServicios implements Comparable<FechaServicios>{
	
	private String fecha;
	private List<Servicio> serviciosAsociados;
	private int numServicios;
	
	public FechaServicios(String pfecha){
		fecha = pfecha;
		serviciosAsociados = new List<Servicio>();
		numServicios = 0;
	}
	
	public String getFecha() {
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public LinkedList<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void agregarServicioAsociado(Servicio s){
		serviciosAsociados.agregar(s);
	}
	public void setServiciosAsociados(List<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	
	public int getNumServicios() {
		return numServicios;
	}
	public void setNumServicios() {
		this.numServicios = serviciosAsociados.darLongitud();
	}
	@Override				
	public int compareTo(FechaServicios o) {
		//Compara por fecha.
		int comp = 0;
		if(this.getFecha().compareTo(o.getFecha())>0){
			comp = 1;
		}
		else if(this.getFecha().compareTo(o.getFecha())<0){
			comp = -1;
		}
		return comp;
	}
}
