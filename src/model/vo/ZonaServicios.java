package model.vo;

import model.data_structures.*;

public class ZonaServicios implements Comparable<ZonaServicios>{

	private String idZona;
	
	private List<FechaServicios> fechasServicios;
	
	public ZonaServicios(String pId){
		idZona = pId;
		fechasServicios = new List<FechaServicios>();
	}
	
	public void agregarFechaServicio(FechaServicios pFecha){
		fechasServicios.agregarEnOrden(pFecha);
	}

	public String getIdZona() {
		return idZona;
	}



	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}



	public LinkedList<FechaServicios> getFechasServicios() {
		return fechasServicios;
	}



	public void setFechasServicios(List<FechaServicios> fechasServicios) {
		this.fechasServicios = fechasServicios;
	}



	@Override
	public int compareTo(ZonaServicios o) {
		//Compara por Identificador.
		int comp = 0;
		if(this.getIdZona().compareTo(o.getIdZona())>0){
			comp = 1;
		}
		else if(this.getIdZona().compareTo(o.getIdZona())<0){
			comp = -1;
		}
		return comp;
	}
}
