package model.vo;

import model.data_structures.*;

public class ServiciosValorPagado {
	
	private List<Servicio> serviciosAsociados;
	private double valorAcumulado;
	
	public ServiciosValorPagado(){
		serviciosAsociados = new List<Servicio>();
		valorAcumulado = 0;
	}
	
	public List<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(List<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado() {
		return valorAcumulado;
	}
	
	public void agregarServicioAsociado(Servicio s){
		serviciosAsociados.agregar(s);
	}
	public void setValorAcumulado() {
		valorAcumulado=0;
		Nodo<Servicio> actual = serviciosAsociados.darPrimero();
		while(actual!=null){
			Servicio actual2 = actual.darElemento();
			valorAcumulado+=actual2.getTripTotal();
			actual = actual.darSiguiente();
		}
	}
	
	

}
