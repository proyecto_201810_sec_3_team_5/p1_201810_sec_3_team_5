package model.vo;

import model.data_structures.*;

public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private LinkedList<Taxi> taxisInscritos;	
	
	public Compania (String pNombre){
		nombre = pNombre;
		taxisInscritos = new List<Taxi>();
	}

	public String getNombre() {
		return nombre;
	}

	public LinkedList<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void agregarTaxi(Taxi nuevo) {
		this.taxisInscritos.agregar(nuevo);
	}
	
	public Taxi darTaxiMasRentable(){
		Taxi masRentable = null;
		List<Taxi> taxis = (List<Taxi>) taxisInscritos;
		Nodo<Taxi> actual = taxis.darPrimero();
		while(actual!=null){
			Taxi tActual = actual.darElemento();
			if(masRentable == null || tActual.darRentabilidad()>masRentable.darRentabilidad()){
				masRentable = tActual;
			}
			actual = actual.darSiguiente();
		}
		return masRentable;
	}

	
	public int compareTo(Compania o) {
		//Compara las compa�ias por nombre
		int res = 0;
		if(this.nombre.compareTo(o.getNombre())>0){
			res = 1;
		}
		else if(this.nombre.compareTo(o.getNombre())<0){
			res = -1;
		}
		return res;
	}
}
