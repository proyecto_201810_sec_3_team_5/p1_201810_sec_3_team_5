package model.logic;


import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.List;
import model.data_structures.Nodo;
import model.data_structures.Queue;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	private List<Servicio> lista;
	private List<Taxi> listaTaxis;
	private List<Compania> listaCompanias;
	
	public boolean cargarSistema(String direccionJson) {
		boolean cargado = false;
		lista = new List<Servicio>();
		listaTaxis = new List<Taxi>();
		listaCompanias = new List<Compania>();
		
		System.out.println("Cargando datos de  " + direccionJson);
		System.out.println("\n-------------------------------------------------------");
		JsonParser parser = new JsonParser();
		try{
			JsonArray arr = (JsonArray) parser.parse(new FileReader(direccionJson));
			for (int i = 0; arr != null && i < arr.size(); i++){
				JsonObject obj= (JsonObject) arr.get(i);
			
				String id = "Unknown";
				if ( obj.get("taxi_id") != null ){
					id = obj.get("taxi_id").getAsString();}
		
				String comp = "Unknown";
				if ( obj.get("company") != null ){ 
					comp = obj.get("company").getAsString();}
		
				String dca = "Unknown";
				if ( obj.get("dropoff_community_area") != null ){
					dca = obj.get("dropoff_community_area").getAsString(); }
				
				String pca = "Unknown";
				if ( obj.get("pickup_community_area") != null ){
					pca = obj.get("pickup_community_area").getAsString(); }
		
				String tripid = "Unknown";
				if ( obj.get("trip_id") != null ){
					tripid = obj.get("trip_id").getAsString(); }
				
				String tripst = "Unknown";
				if ( obj.get("trip_start_timestamp") != null ){
					tripst = obj.get("trip_start_timestamp").getAsString(); }
				
				String tripet = "Unknown";
				if ( obj.get("trip_end_timestamp") != null ){
					tripet = obj.get("trip_end_timestamp").getAsString(); }
		
				int tripseconds = -1;
				if ( obj.get("trip_seconds") != null ){
					tripseconds = obj.get("trip_seconds").getAsInt(); }
		
				double tripmiles = -1;
				if ( obj.get("trip_miles") != null ){
					tripmiles = obj.get("trip_miles").getAsDouble(); }
		
				double triptotal = -1;
				if ( obj.get("trip_total") != null ){ 
					triptotal = obj.get("trip_total").getAsDouble(); }
				
				//Carga de servicios
				Servicio nuevo = new Servicio(id, comp, dca, pca, tripid, tripst, tripet, tripseconds, tripmiles, triptotal);
				lista.agregar(nuevo);
			}
			
			//Carga de taxis de acuerdo a servicios cargados
			Nodo<Servicio> actual = lista.darPrimero();
			while(actual!=null){
				if(!actual.darElemento().getTaxiId().equals("Unknown"))
				{
					Taxi temp = new Taxi(actual.darElemento().getTaxiId(),actual.darElemento().getCompany());
					Taxi existente = listaTaxis.elementoExistente(temp);
					if(existente == null){
						listaTaxis.agregar(temp);
						listaTaxis.darUltimo().darElemento().agregarServicio(actual.darElemento());
					}
					else{
						existente.agregarServicio(actual.darElemento());
					}
				}
				actual = actual.darSiguiente();
			}
			
			//Carga de Compa�ias de acuerdo a taxis cargados
			Nodo<Taxi> actualC = listaTaxis.darPrimero();
			while(actualC!=null){
				if(!actualC.darElemento().getCompany().equals("Unknown"))
				{
					Compania temp = new Compania(actualC.darElemento().getCompany());
					Compania existente = listaCompanias.elementoExistente(temp);
					if(existente == null){
						temp.agregarTaxi(actualC.darElemento());
						listaCompanias.agregarEnOrden(temp);
					}
					else{
						existente.agregarTaxi(actualC.darElemento());
					}
				}
				actualC = actualC.darSiguiente();
			}
			cargado = true;
			System.out.println("\nSe cargaron "+lista.darLongitud()+" servicios.");
			System.out.println("Se cargaron "+listaTaxis.darLongitud()+" taxis.");
			System.out.println("Se cargaron "+listaCompanias.darLongitud()+" compa��as.");
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();}
		return cargado;
	}
	
	public List<Servicio> serviciosPorRTiempo(RangoFechaHora rango){
		List<Servicio> Cservicios= null;
		Nodo<Servicio> actual= lista.darPrimero();
		String rangoI=rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String rangoF=rango.getFechaFinal()+"T"+rango.getHoraFinal();
		while(!actual.darSiguiente().equals(null)) {
			int compare= actual.darElemento().getStartTimeC().compareTo(rangoI);
			if(compare>0) {
				return Cservicios;
			}				
			if(actual.darElemento().getStartTime().equals(rangoI)){
				Cservicios.agregar(actual.darElemento());			
			}				
			if(actual.darElemento().getStartTimeC().compareTo(rangoI)<=0 && actual.darElemento().getEndTimeC().compareTo(rangoF)>=0) {
				Cservicios.agregar(actual.darElemento());
			}
		actual=actual.darSiguiente();
		}
		return Cservicios;
	}
	
	public List<Servicio> ordenarSPorRTiempo(List<Servicio> listaC,RangoFechaHora rango){
		Nodo<Servicio> actual= listaC.darPrimero();
		Nodo<Servicio> prim= listaC.darPrimero();

		boolean done=false;
			while(!done) {
				done=true;
				actual=listaC.darPrimero();
				while (actual.darSiguiente()!=null) {
					Nodo<Servicio> sig= actual.darSiguiente();
					Nodo<Servicio> ant= actual.darAnterior();
					if(actual==prim && actual.darElemento().compareTime(actual.darSiguiente().darElemento())<0) {
						actual.cambiarSiguiente(sig.darSiguiente());
						sig.cambiarSiguiente(actual);
						sig.cambiarAnterior(actual.darAnterior());
						actual.cambiarAnterior(sig);
						listaC.cambiarPrimero(sig);
						done=false;
					}
					if(actual.darElemento().compareTime(actual.darSiguiente().darElemento())<0){
						actual.cambiarSiguiente(sig.darSiguiente());
						sig.cambiarSiguiente(actual);
						sig.cambiarAnterior(actual.darAnterior());
						actual.cambiarAnterior(sig);	
						done=false;
					}

					actual=actual.darSiguiente();

			}
		}
		return listaC;
	
	}
		public List<Servicio> ordenarSPorDistancia(List<Servicio> listaC){
		Nodo<Servicio> actual= listaC.darPrimero();
		Nodo<Servicio> prim= listaC.darPrimero();

		boolean done=false;
			while(!done) {
				done=true;
				actual=listaC.darPrimero();
				while (actual.darSiguiente()!=null) {
					Nodo<Servicio> sig= actual.darSiguiente();
					Nodo<Servicio> ant= actual.darAnterior();
					if(actual==prim && actual.darElemento().compareDistance(actual.darSiguiente().darElemento())<0) {
						actual.cambiarSiguiente(sig.darSiguiente());
						sig.cambiarSiguiente(actual);
						sig.cambiarAnterior(actual.darAnterior());
						actual.cambiarAnterior(sig);
						listaC.cambiarPrimero(sig);
						done=false;
					}
					if(actual.darElemento().compareDistance(actual.darSiguiente().darElemento())<0){
						actual.cambiarSiguiente(sig.darSiguiente());
						sig.cambiarSiguiente(actual);
						sig.cambiarAnterior(actual.darAnterior());
						actual.cambiarAnterior(sig);	
						done=false;
					}

					actual=actual.darSiguiente();

			}
		}
		return listaC;
	
	}
	
	@Override //1A
    /*Generar	una	Cola con	todos	los	servicios	de	taxi	que	se	prestaron	en	un	periodo	de	
	*tiempo	 dado	 por	 una	 fecha/hora	 inicial	 y	 una	 fecha/hora	 final	 de	 consulta.	 El	 inicio	 y	
	*terminaci�n	del	servicio	debe	estar	incluido dentro	del	periodo	de	consulta.	Los	servicios	
	*deben	mostrarse	en	orden	cronol�gico	de	su	fecha/hora	inicial.	      
    */
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		List<Servicio> h = null;
		h= ordenarSPorRTiempo(serviciosPorRTiempo(rango),rango);
		List<Servicio> Cservicios= null;
		Queue<Servicio> Qservicios= null;
		Nodo<Servicio> actual= h.darPrimero();			
		
		while(actual.darSiguiente()!=null) {
			Qservicios.enqueue(actual.darElemento());
			actual=actual.darSiguiente();
		}
		return Qservicios;
	}

	@Override //2A
//	Buscar	el	taxi	de	una	compa��a	dada	que	m�s	servicios	inici�	en	un	periodo	de	tiempo	
//	dado	por	una	fecha/hora	inicial	y	una	fecha/hora	final	de	consulta.	
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		// TODO Auto-generated method stub
		List<Servicio> sT= serviciosPorRTiempo(rango);
		List<Servicio> sTC=null;
		Nodo<Servicio> actual=sT.darPrimero();
		List<String> idT=null;
		List<Taxi> tx=null;

		while(actual.darSiguiente()!=null) {
			if(actual.darElemento().getCompany().equals(company)) {
				sTC.agregar(actual.darElemento());
			}
		}
		Nodo<Servicio>actual2=sTC.darPrimero();
		while(actual2.darSiguiente()!=null) {
			if(idT.elementoExistente(actual2.darElemento().getTaxiId())!=null) {
				idT.agregar(actual2.darElemento().getTaxiId());

			}
		
		}
		Taxi t= null;
		Nodo<String> actual3=idT.darPrimero();
		while(actual3.darSiguiente()!=null) {
		
			Nodo<Taxi>actual4=listaTaxis.darPrimero();
			while(actual4.darSiguiente()!=null) {
				if(actual4.darElemento().getCompany().equals(company)&& actual4.darElemento().getTaxiId().equals(actual3.darElemento())) {
					if(t!=null) {
						if(t.numServicios(rango)<actual4.darElemento().numServicios(rango)) {
							t=actual4.darElemento();
						}
					}
					else {
						t=actual4.darElemento();
					}
				}
			}
		}
		
		return t;
	}

	@Override //3A
//	Buscar	la	informaci�n	completa	de	un	taxi,	a	partir	de	su	identificador,	en	un	periodo	
//	de	tiempo	dado	por	una	fecha/hora	inicial	y	una	fecha/hora	final	de	consulta.	Incluye	el	
//	nombre	de	su	compa��a	y	los	valores	totales	de	plata	ganada,	de servicios	prestados,	de	
//	distancia	recorrida	y	de	tiempo	total	de	servicios.
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		Taxi taxiA=null;
		Nodo<Taxi> nT= listaTaxis.darPrimero();
		while(nT.darSiguiente()!=null) {
			if(nT.darElemento().getTaxiId().equals(id)) {
				taxiA=nT.darElemento();
				break;
			}
		}
		InfoTaxiRango infoTaxi= new InfoTaxiRango();
		infoTaxi.setCompany(taxiA.getCompany());
		infoTaxi.setServiciosPrestadosEnRango(taxiA.serviciosPorRTiempoTaxi(rango));
		infoTaxi.setPlataGanada(taxiA.darPalataRango(rango));
		infoTaxi.setDistanciaTotalRecorrida(taxiA.darDistanciaRango(rango));
		infoTaxi.setTiempoTotal(Double.toString(taxiA.darDuracionRango(rango)));
		
		
		// TODO Auto-generated method stub
		return infoTaxi;
		
	}

	@Override //4A
//	Retornar	una	lista	de	rangos	de	distancia	recorrida,	en	la	que	se	encuentran	todos	los	
//	servicios	de	taxis	servidos	por	las	compa��as,	en	una	fecha	dada	y	en	un	rango	de	horas	
//	especificada.		La	informaci�n	debe	estar	ordenada	por	la	distancia	recorrida,	as�	la	primera	
//	posici�n	de	la	lista	tiene	a	su	vez	una	lista	con	todos	los	servicios	cuya	distancia	recorrida	
//	esta	entre	[0	 y	1)	milla.	En	la	segunda	posici�n,	los	recorridos	entre	[1	 y	2)	millas,	 y	as�	
//	sucesivamente.							
	public LinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		// TODO Auto-generated method stub
		RangoFechaHora rango= new RangoFechaHora(fecha,fecha, horaInicial, horaFinal);
		List<Servicio> h= serviciosPorRTiempo(rango);
		List<Servicio> hO=ordenarSPorDistancia(h);
		Nodo<Servicio> actual=hO.darPrimero();
		List<RangoDistancia> rd= null;
		Nodo<RangoDistancia> actual2=null;
		List<Servicio> lsr=null;
		
		double limsup=1;
		while(actual.darSiguiente()!=null) {
			
			if( limsup<actual.darElemento().getTripMiles()) {
				
				RangoDistancia f=null;
				f.setLimineInferior(limsup-1);
				f.setLimiteSuperior(limsup);
				List<Servicio> lsr2=null;
				Nodo<Servicio> actual3=lsr.darPrimero();
				while(actual3.darSiguiente()!=null) {
					lsr2.agregar(actual3.darElemento());
				}
				f.setServiciosEnRango(lsr2);					
				rd.agregar(f);
				limsup++;
			lsr=null;
;
			}
			else {

				lsr.agregar(actual.darElemento());
				
			}
		}
			
		
		return rd;
	}

	public LinkedList<Taxi> darListaTaxis()
	{
		return listaTaxis;
	}
	@Override //1B
	public LinkedList<Compania> darCompaniasTaxisInscritos(){
		
		return listaCompanias;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		boolean encontro = false;
		Taxi mayor = null;
		double mayorFacturacion = 0;
		List<Compania> companias = listaCompanias;
		Nodo<Compania> nodoActual = companias.darPrimero();
		while(nodoActual != null && !encontro){
			Compania actual = nodoActual.darElemento();
			if(actual.getNombre().equals(nomCompania)){
				List<Taxi> taxis = (List<Taxi>) actual.getTaxisInscritos();
				Nodo<Taxi> nodoTaxi = taxis.darPrimero();
				while(nodoTaxi!= null){
					Taxi taxiActual = nodoTaxi.darElemento();
					if(taxiActual.darFacturacion(rango)>mayorFacturacion){
						mayorFacturacion = taxiActual.darFacturacion(rango);
						mayor = taxiActual;
					}
					nodoTaxi = nodoTaxi.darSiguiente();
				}
			}
			else{
			nodoActual = nodoActual.darSiguiente();
			}
		}
		return mayor;
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		ServiciosValorPagado consOtra = new ServiciosValorPagado();
		ServiciosValorPagado otraCons = new ServiciosValorPagado();
		ServiciosValorPagado consCons = new ServiciosValorPagado();
		List<Servicio> servicios = lista;
		Nodo<Servicio> actual = servicios.darPrimero();
		while(actual!=null){
			Servicio sActual = actual.darElemento();
			if(sActual.getStartTime()[0].compareTo(rango.getFechaInicial())>=0 && sActual.getStartTime()[1].compareTo(rango.getHoraInicio())>=0 && sActual.getEndTime()[0].compareTo(rango.getFechaFinal())<=0 && sActual.getEndTime()[1].compareTo(rango.getHoraFinal())<=0)
			{
				if(sActual.getPickupCommunityArea().equals(idZona) && !sActual.getCommunityArea().equals(idZona)){
					consOtra.agregarServicioAsociado(sActual);
				}
				else if(!sActual.getPickupCommunityArea().equals(idZona) && sActual.getCommunityArea().equals(idZona)){
					otraCons.agregarServicioAsociado(sActual);
				}
				else if(sActual.getPickupCommunityArea().equals(idZona) && sActual.getCommunityArea().equals(idZona)){
					consCons.agregarServicioAsociado(sActual);
				}
			}
			actual = actual.darSiguiente();
		}
		consOtra.setValorAcumulado();
		otraCons.setValorAcumulado();
		consCons.setValorAcumulado();
		ServiciosValorPagado[] res = new ServiciosValorPagado[3];
		res[0]=consOtra;
		res[1]=otraCons;
		res[2]=consCons;
		return res;
	}

	@Override //4B
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		List<Servicio> servicios = lista;
		List<ZonaServicios> zonas = new List<ZonaServicios>();
		Nodo<Servicio> actual = servicios.darPrimero();
		int zonaActual = 0;
		while(zonaActual < 100){
			while(actual!=null){
				Servicio sActual = actual.darElemento();
				if(sActual.getStartTime()[0].compareTo(rango.getFechaInicial())>=0 && sActual.getStartTime()[1].compareTo(rango.getHoraInicio())>=0 && sActual.getEndTime()[0].compareTo(rango.getFechaFinal())<=0 && sActual.getEndTime()[1].compareTo(rango.getHoraFinal())<=0)
				{
					ZonaServicios zonaServ = new ZonaServicios(String.valueOf(zonaActual));
					FechaServicios fechaServ = new FechaServicios(sActual.getStartTime()[0]);
					if(!sActual.getPickupCommunityArea().equals("Unknown")){
						if(Integer.parseInt(sActual.getPickupCommunityArea()) == zonaActual){
							if(zonas.elementoExistente(zonaServ)==null){
								zonaServ.agregarFechaServicio(fechaServ);
								zonas.agregar(zonaServ);
							}
							else{
								zonas.elementoExistente(zonaServ).agregarFechaServicio(fechaServ);
							}
						}
					}
				}
				actual = actual.darSiguiente();
			}
		zonaActual++;
		actual = servicios.darPrimero();
		}
		return zonas;
	}

	@Override //2C
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //3C
	public List<CompaniaTaxi> taxisMasRentables()
	{
		List<CompaniaTaxi> res = new List<CompaniaTaxi>();
		List<Compania> comps = listaCompanias;
		Nodo<Compania> actual = comps.darPrimero();
		while(actual != null){
			Compania cActual = actual.darElemento();
			Taxi masRentAct = cActual.darTaxiMasRentable();
			CompaniaTaxi nueva = new CompaniaTaxi();
			nueva.setNomCompania(cActual.getNombre());
			nueva.setTaxi(masRentAct);
			res.agregar(nueva);
			actual = actual.darSiguiente();
		}
		return res;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

}
